package com.okky.test.testingcallapi.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "test")
@Setter
@Getter
public class Test {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "test_idgen", sequenceName = "test_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_idgen")
    private Long id;

    private int userId;
    private String title;
    private String body;
}
