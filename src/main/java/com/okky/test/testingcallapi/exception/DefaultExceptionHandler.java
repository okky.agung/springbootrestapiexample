package com.okky.test.testingcallapi.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Map;

@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, String>> handleException(Exception ex) {
        log.error("Unexpected error occurred!", ex);
        return ResponseEntity.internalServerError().body(Map.of("message", ex.getMessage()));
    }

    @ResponseBody
    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<String> handleHttpClientErrorException(HttpClientErrorException ex) {
        return ResponseEntity.status(ex.getStatusCode()).body(ex.getResponseBodyAsString());
    }

    @ExceptionHandler(HttpStatusBaseException.class)
    public ResponseEntity<String> handleHttpStatusBaseException(HttpStatusBaseException ex) {
        return ResponseEntity.status(ex.getHttpStatus()).body(String.format(ex.getMessage(), ex.getArguments()));
    }

}
