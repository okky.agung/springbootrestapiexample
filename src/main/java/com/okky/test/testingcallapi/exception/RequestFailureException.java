package com.okky.test.testingcallapi.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class RequestFailureException extends RuntimeException {
    private final String message;
    private final Throwable cause;
}
