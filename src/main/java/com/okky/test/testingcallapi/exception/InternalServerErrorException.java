package com.okky.test.testingcallapi.exception;

import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends HttpStatusBaseException {

    public InternalServerErrorException(String message, Object... arguments) {
        super(message, arguments);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
