package com.okky.test.testingcallapi.exception;

public class PrivilageException extends RuntimeException{
    private String message;

    public PrivilageException(String message) {
        super(message);
        this.message = message;
    }

    public PrivilageException() {
    }
}
