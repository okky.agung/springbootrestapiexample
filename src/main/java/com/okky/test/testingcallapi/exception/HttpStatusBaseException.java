package com.okky.test.testingcallapi.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public abstract class HttpStatusBaseException extends RuntimeException {

    private final String message;
    private final Object[] arguments;

    public abstract HttpStatus getHttpStatus();
}
