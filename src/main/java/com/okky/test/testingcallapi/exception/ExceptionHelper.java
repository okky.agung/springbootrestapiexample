package com.okky.test.testingcallapi.exception;

import com.okky.test.testingcallapi.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ExceptionHelper {

    @ExceptionHandler(value = { Exception.class })
    public ResponseEntity<Object> handleException(Exception ex) {
        ResponseObject responseObject = new ResponseObject();
        log.error("Exception: ",ex.getMessage());
        responseObject.setContent(null);
        responseObject.setMessage(ex.getMessage());
        responseObject.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        return new ResponseEntity<Object>(responseObject,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = { EntityNotFoundException.class })
    public ResponseEntity<Object> handleEntityNotFoundException(Exception ex) {
        ResponseObject responseObject = new ResponseObject();
        log.error("Exception: ",ex.getMessage());
        responseObject.setContent(null);
        responseObject.setMessage(ex.getMessage());
        responseObject.setStatus(HttpStatus.NOT_FOUND.toString());
        return new ResponseEntity<>(responseObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { BadRequestException.class })
    public ResponseEntity<Object> handleBadRequestException(Exception ex) {
        ResponseObject responseObject = new ResponseObject();
        log.error("Exception: ",ex.getMessage());
        responseObject.setContent(null);
        responseObject.setMessage(ex.getMessage());
        responseObject.setStatus(HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<>(responseObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { PrivilageException.class })
    public ResponseEntity<Object> handlePrivilageException(Exception ex) {
        ResponseObject responseObject = new ResponseObject();
        log.error("Exception: ",ex.getMessage());
        responseObject.setContent(null);
        responseObject.setMessage(ex.getMessage());
        responseObject.setStatus(HttpStatus.FORBIDDEN.toString());
        return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = { MethodArgumentNotValidException.class })
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        ResponseObject responseObject = new ResponseObject();

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        log.error("Exception: ",errors);
        responseObject.setContent(null);
        responseObject.setMessage(errors.toString());
        responseObject.setStatus(HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<Object>(responseObject,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class })
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setContent(null);
        responseObject.setMessage(ex.getMessage());
        responseObject.setStatus(HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<Object>(responseObject,HttpStatus.BAD_REQUEST);
    }
}
