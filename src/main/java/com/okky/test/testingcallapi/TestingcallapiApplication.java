package com.okky.test.testingcallapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingcallapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingcallapiApplication.class, args);
	}

}
