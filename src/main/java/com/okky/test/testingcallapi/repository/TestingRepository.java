package com.okky.test.testingcallapi.repository;

import com.okky.test.testingcallapi.dto.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestingRepository extends JpaRepository<Test,Long> {
}
