package com.okky.test.testingcallapi.model;

import lombok.Data;

@Data
public class ResponseData {
    int userId;
    int id;
    String title;
    String body;
}
