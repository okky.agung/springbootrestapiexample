package com.okky.test.testingcallapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseObject<T> {
    private String status;
    private String message;
    private String[] code;
    private T content;

    @JsonIgnore
    HttpStatus statusCode;

    public ResponseObject(HttpStatus statusCode, String status){
        this.statusCode = statusCode;
        this.status = status;
        this.message = "";
    }

    public ResponseObject(HttpStatus statusCode, String status, T content){
        this.statusCode = statusCode;
        this.status = status;
        this.content = content;
        this.message = "";
    }

    public ResponseObject(HttpStatus statusCode, String status, String message){
        this.statusCode = statusCode;
        this.status = status;
        this.message = message;
    }

    public ResponseObject(HttpStatus statusCode, String status, String message, T content){
        this.statusCode = statusCode;
        this.message = message;
        this.status = status;
        this.content = content;
    }

        public ResponseObject(HttpStatus statusCode, String status, String message, String[]code){
        this.statusCode = statusCode;
        this.message = message;
        this.status = status;
        this.code = code;
    }

    public ResponseObject(HttpStatus statusCode, String status, String message, String[]code, T content){
        this.statusCode = statusCode;
        this.message = message;
        this.status = status;
        this.content = content;
        this.code = code;
    }
}
