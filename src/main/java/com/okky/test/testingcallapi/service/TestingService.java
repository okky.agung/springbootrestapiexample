package com.okky.test.testingcallapi.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.okky.test.testingcallapi.config.Constant;
import com.okky.test.testingcallapi.config.Sender;
import com.okky.test.testingcallapi.dto.Test;
import com.okky.test.testingcallapi.model.ResponseData;
import com.okky.test.testingcallapi.model.ResponseObject;
import com.okky.test.testingcallapi.repository.TestingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestingService {
    Gson g = new Gson();

    @Value("${url.data}")
    private String urlData;

    private final TestingRepository testingRepository;


    public ResponseObject getData() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        try{
            URL obj = new URL(urlData);
            String test = Sender.httpsGetData(obj);

            List<ResponseData> testResponse = gson.fromJson(test, ArrayList.class);
            System.out.println(gson.toJson(testResponse));
            try{
//                for (int i = 0; i < testResponse.size(); i++) {
//                    var dat = new Test();
//                    dat.setTitle(testResponse.get(i).getTitle());
//                    dat.setBody(testResponse.get(i).getBody());
//                    dat.setUserId(testResponse.get(i).getUserId());
//                    testingRepository.save(dat);
//                }

                return new ResponseObject(HttpStatus.OK, Constant.STATUS.SUCCESS, "Success Get Data", testResponse);

            }catch (IllegalArgumentException e){
                return new ResponseObject(HttpStatus.BAD_REQUEST, Constant.STATUS.FAILED, e.getMessage());
            }
        }catch (Exception E){
            return new ResponseObject(HttpStatus.BAD_REQUEST, Constant.STATUS.FAILED, E.getMessage());
        }
    }
}
