package com.okky.test.testingcallapi.config;

import com.google.gson.Gson;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;


/**
 * Created by Okky on 9/16/2019.
 */
public class Sender {
    protected static RestTemplate restTemplate;

    public static String httpsGetData(URL obj) throws IOException {
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        StringBuffer response = new StringBuffer();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        } else {
            System.out.println("GET request not worked");
        }
        return response.toString();
    }

    public static Object httpsGetDataWithAuth(String url,String authorization) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.put("Authorization", Collections.singletonList(authorization));
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        String uri = UriComponentsBuilder.fromHttpUrl(url).build()
                .toUriString();
        restTemplate = new RestTemplate();
        ResponseEntity<Object> response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Object.class);
        Gson g = new Gson();
        return g.toJson(response.getBody());
    }

}
