package com.okky.test.testingcallapi.config;

public class Constant {
    public class STATUS {
        public static final String SUCCESS = "SUCCESS";
        public static final String FAILED = "FAILED";
    }
}
