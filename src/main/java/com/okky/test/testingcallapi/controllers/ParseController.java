package com.okky.test.testingcallapi.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.okky.test.testingcallapi.config.Constant;
import com.okky.test.testingcallapi.config.Sender;
import com.okky.test.testingcallapi.dto.Test;
import com.okky.test.testingcallapi.model.ResponseData;
import com.okky.test.testingcallapi.model.ResponseObject;
import com.okky.test.testingcallapi.service.TestingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RequiredArgsConstructor
@RestController
public class ParseController {

    private final TestingService testingService;

    @GetMapping("/getData")
    public ResponseObject getData() throws IOException {
        return testingService.getData();
    }

}
